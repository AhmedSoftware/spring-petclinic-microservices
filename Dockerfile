FROM docker/compose
VOLUME /tmp
COPY ./docker-compose.yml .
COPY ./docker ./docker

EXPOSE 30000
ENTRYPOINT ["docker-compose", "up", "-d"]
